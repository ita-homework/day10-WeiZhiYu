package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(EmployeeJPARepository employeeJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<Employee> findAll() {
        return employeeJPARepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeJPARepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public Employee update(Long id, EmployeeRequest EmployeeRequest) {
        Employee toBeUpdatedEmployee = findById(id);
        if (EmployeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(EmployeeRequest.getSalary());
        }
        if (EmployeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(EmployeeRequest.getAge());
        }
        return employeeJPARepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public Employee create(EmployeeRequest employeeRequest) {
        return employeeJPARepository.save(EmployeeMapper.toEntity(employeeRequest));
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeJPARepository.findAll(PageRequest.of(page - 1, size)).toList();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
